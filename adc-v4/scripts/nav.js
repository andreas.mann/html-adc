const nav_map = new Map();

// nav_map.set('Home','index.html');
// nav_map.set('Home','index.html');
nav_map.set('Home','index.html');
nav_map.set('Service','index.html#ANC-Service');
nav_map.set('Protected Access','index.html#ANC-ProtAcc');
nav_map.set('Data Privacy Protection','privacy-protection.html');
nav_map.set('repertoire public','https://scireptor.dkfz.de/airr/v1/repertoire');
nav_map.set('repertoire protected','https://adc.dkfz.de/airr/v1/repertoire');

function write_top_nav(){
	//alert('test');
	//document.getElementById('topnav').innerHTML = '<a class="active" href="index.html">Home</a> |';
	for (const [key, value] of nav_map.entries() ) {
		//var content = document.createTextNode ( '<a href="'+ value +'" ">'+ key +'</a> | ' );   
		//document.getElementById('topnav').appendChild( content )  ;
		//document.getElementById('topnav').insertAdjacentHTML('beforeend', '<a href="'+ value +'" ">'+ key +'</a> | '); 
		document.getElementById('topnav').insertAdjacentHTML('beforeend', '<li class="nav-item"><a class="nav-link active" href="'+value+'" >['+key+']</a></li>'); 
	}
}
