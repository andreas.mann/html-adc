// configure keycloak and middleware 
//
// cors check initKeycloak() !!! 
var kcl_host_url="https://adc.dkfz.de";
var kcl_auth_url="https://adc.dkfz.de/auth/";
var kcl_realm_name="realm-ADC-prod"
var kcl_realm="https://adc.dkfz.de/auth/realms/realm-ADC-prod"
var mw_rep_end="https://adc.dkfz.de/airr/v1/repertoire"					
var mw_rea_end="https://adc.dkfz.de/airr/v1/rearrangement"		
var adc_api_help_url="https://docs.airr-community.org/en/stable/api/adc_api.html"			

var userTokenVisible = false;

function flapUserToken(){
    if ( userTokenVisible  ) { userTokenVisible = false;  } else { userTokenVisible = true;  }
    if ( userTokenVisible  ) { 
        document.getElementById('show-token-div').style = "display: block";
        document.getElementById('show-token-textarea').innerHTML = keycloak.token;
    }else { document.getElementById('show-token-div').style = "display: none";  }

    if ( userTokenVisible  ){ document.getElementById('switch').innerHTML = "[-]"; }
    else { document.getElementById('switch').innerHTML = "[+]"; }
}

var keycloak = new Keycloak( 
    {
        url:  kcl_auth_url,
        realm: kcl_realm_name,
        clientId: 'mwaccess'
    }
);

keycloak.onAuthSuccess = function() {
    //document.getElementById('btn-login').style = "background-color: LightGreen";
    document.getElementById('sp-user-name').innerHTML = "user: <b>"+keycloak.idTokenParsed.name+"</b>, " 
        + keycloak.idTokenParsed.email + " |  " 
        +  "<a id='switch' href='#' onClick='flapUserToken();' >[-]</a>";


    document.getElementById('show-token-div').style = "display: block";
    userTokenVisible=true; /* true if block, false if none */
    document.getElementById('show-token-textarea').innerHTML = keycloak.token;

    var token_decoded = parseJwt(keycloak.token);
    document.getElementById('show-token-decoded').innerHTML += JSON.stringify(token_decoded,undefined,1);

}

keycloak.onTokenExpired = function() {
    console.log("token expired");
    doLogout();
}

keycloak.onAuthRefreshSuccess = function() {
    console.log("token refreshed: OK");
}

keycloak.onAuthRefreshError = function() {
    console.log("token refreshed: ERROR");
}

function initKeycloak() {

    console.log("hostname: " + window.location.hostname);

    if ( ! window.location.hostname.includes("legolas") ){
        window.location.replace("https://legolas");
        return;
    }

    keycloak.init(
        { onLoad: 'check-sso',  silentCheckSsoRedirectUri: window.location.origin + '/adc/silent-check-sso.html'  }
    )
    .then(function(authenticated) {
        console.log(authenticated ? 'authenticated as '+ keycloak.subject  +' ('+keycloak.idTokenParsed.name+')  ' : 'not authenticated');
        if (!authenticated) { document.getElementById('sp-btn-logout').innerHTML = "";  }
    })
    .catch(function() {
          alert('failed to initialize');
     });
     userTokenVisible = true; 
};

 function check_status(){
    if ( keycloak.authenticated ) {
        console.log('cs:keycloak authenticated');
        document.getElementById('btn-login').style = "background-color: Green";
        //document.getElementById('sp-btn-logout').innerHTML = '<button onClick="keycloak.logout()">logout</button>';
        document.getElementById('sp-user-name').innerHTML = ""+keycloak.idTokenParsed.name+"";
    }else{
        console.log('cs:keycloak not authenticated');
        //document.getElementById('btn-login').style.background-color = "White";
        document.getElementById('btn-login').style = "background-color: White";
        //document.getElementById('sp-btn-logout').innerHTML = "";
        document.getElementById('sp-user-name').innerHTML = "";
    }
};

function doLogin(){
        //document.getElementById('btn-login').style = "background-color: Yellow";
        document.getElementById('sp-user-name').innerHTML = ""+keycloak.idTokenParsed.name+"";
        userTokenVisible = true;
        //document.getElementById('show-token-div').style = "display: none";
}	
    
function doLogout(){
        //document.getElementById('btn-login').style = "background-color: White";
        document.getElementById('sp-user-name').innerHTML = "";
        document.getElementById('sp-btn-logout').innerHTML = "";
        document.getElementById('show-token-div').style = "display: none";
        //flapUserToken();
        obj = null;
        obj_rea = null;
        obj_rep = null;
}	
    
/*
https://stackoverflow.com/questions/13304471/javascript-get-code-to-run-every-minute
*/
function refreshToken(){
    keycloak.updateToken(120)
    .then(function(refreshed) {
        if (refreshed) {
            console.log('Token was successfully refreshed');
        } else {
            console.log('Token is still valid');
        }
    }).catch(function() {
        console.log('Failed to refresh the token, or the session has expired');
    });    
    document.getElementById('sp-user-name').innerHTML = ""+keycloak.idTokenParsed.name+"";
}


var permissionTicket = null;
var RPT = null;
var flowError = null;
var body_post_data = '';
var log_msg = 'OK';
var obj = null;
var obj_rep = null;
var obj_rea = null;
var elems_repertoire = 0;
var elems_rearrangement = 0;

/*
    method GET|POST
    endpoint repertoire|rearrangement
*/ 
function getFromEnd(method, endpoint){
    log_msg="OK"; flowError=null;
    document.getElementById('response_body').innerHTML='<img src="images/loading-loading-forever.gif" alt="LOADING..." width="25" height="25">';

    var body_post_data_id = 'body_data_rep'; if (endpoint == 'rearrangement') { body_post_data_id = 'body_data_rea'; }
    body_post_data=document.getElementById(body_post_data_id).value;
    if ( body_post_data == null ) { body_post_data='{}'; }
    console.log('body_post_data: ' + body_post_data + ', lengt: ' + document.getElementById(body_post_data_id).value.length);


    if ( method == 'GET' && endpoint == 'rearrangement' ) {
        if ( document.getElementById(endpoint).value == '' ) { alert(endpoint + ': GET requires sequence_id ('+adc_api_help_url+')'); return; }
    }

    getPermissionTicket(method, endpoint);
    //console.log('permissionTicket='+permissionTicket);
    if ( flowError == null ) { requestRPT(); }
    //console.log('RPT='+RPT);
    if ( flowError == null ) { requestData(method, endpoint); }


    document.getElementById('status').value=log_msg;
    document.getElementById('displayDownloadLink').style = "display: none";

    if ( flowError == true ) { flowError = null; document.getElementById('response_body').innerHTML=''; return;}	

    // data has been retrieved 

    var elems = 0;
    try {
        if ( endpoint == 'repertoire' ) { elems = Object.keys(obj.Repertoire).length; elems_repertoire = elems; }
        if ( endpoint == 'rearrangement' ) { elems = Object.keys(obj.Rearrangement).length; elems_rearrangement = elems; }    
        log_msg=log_msg + ', '+elems+' record(s) retrieved.'
    }
    catch (exception) { log_msg=log_msg + ', obj (data) is undefined or null (missing permission?)'; }
	document.getElementById('status').value=log_msg;

    // demo table
    var divContainer = document.getElementById("TableRep"); divContainer.innerHTML = "";    
    if ( endpoint == 'repertoire' ) { writeTableRep(obj); }
    else {
        if ( obj_rep != null ) { writeTableRep(obj_rep); }
    }

    //jumpto("ANC-response_body");
    jumpto("ANC-response_body_raw");
}

function getPermissionTicket(method, endpoint  ){
    var Id = document.getElementById(endpoint).value;
    
    if (Id != '') { Id = '/'+Id  }
    if (method == 'POST') { Id = ''; }	
    var url = mw_rep_end + Id;
    if (endpoint == 'rearrangement') { url = mw_rea_end + Id  }
    
    console.log("url: " + url);

    var xhr = new XMLHttpRequest();
    xhr.open(method, url, false);
    xhr.setRequestHeader('Content-Protected', 'true');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.setRequestHeader('crossorigin', 'anonymous');
    xhr.onerror = function () {
        log_msg='** error: getPermissionTicket';console.log(log_msg); flowError = true;
    };

    xhr.onreadystatechange = function () {
        if ( xhr.status == 400 ){ log_msg='** error getPermissionTicket (check query json)'; console.log(log_msg); flowError = true; return;}
        if (this.readyState == 4) {
            //alert('Success');
            var wwwAuthHeader = xhr.getResponseHeader('WWW-Authenticate');
            if ( wwwAuthHeader == null ) { log_msg='** error getPermissionTicket: missing WWW-Authenticate'; console.log(log_msg); flowError = true; return; }
            //console.log(xhr.getAllResponseHeaders());
            //alert(wwwAuthHeader);
            var regex = /^.*ticket="(.*)"$/;
            permissionTicket = /^.*ticket="(.*)"$/.exec(wwwAuthHeader)[1];
            //var ticket = wwwAuthHeader.match(regex);
            //console.log('ticket='+permissionTicket);
            
        }
    }	
    if (method == 'POST'){ xhr.send(  body_post_data ); }
    else { xhr.send( ); }
}

function requestRPT( ){
    var xhr = new XMLHttpRequest();
    var url = kcl_realm + '/protocol/openid-connect/token';
    var body_params = 'grant_type=urn:ietf:params:oauth:grant-type:uma-ticket&ticket=' + permissionTicket;
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
    
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onerror = function () {
            log_msg='"** error: requestRPT"'; console.log(log_msg); flowError = true;
        };

        xhr.onreadystatechange = function () {
            if ( xhr.status == 400 ){ log_msg='"** error: requestRPT"'; console.log(log_msg);flowError = true; return; }
            if (this.readyState == 4) {
                var response_body=JSON.parse(xhr.response);
                //console.log(response_body);
                RPT=response_body.access_token	
            }
    }	
    xhr.send( body_params );
}

function requestData(method, endpoint){
    var Id = document.getElementById(endpoint).value
    if (Id != '') { Id = '/'+Id  }
    if (method == 'POST') { Id = ''; }
    
    var url = mw_rep_end + Id;
    if (endpoint == 'rearrangement') { url = mw_rea_end + Id  }
    
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, false);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.setRequestHeader('Authorization', 'Bearer ' + RPT);
    xhr.setRequestHeader('Content-Protected', 'true');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    //xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onerror = function () {
                log_msg="** error: requestData"; console.log(log_msg); flowError = true; return;
        };

        xhr.onreadystatechange = function () {
            if ( xhr.status == 400 ){ log_msg="** error: requestData"; console.log(log_msg);flowError = true; return; }
            if (this.readyState == 4) {
                //var response_body=JSON.parse(xhr.response);
                //console.log(response_body);	
                //document.getElementById('response_body').innerHTML=JSON.stringify(xhr.response);
                obj=JSON.parse(xhr.response);   // string response to object
                document.getElementById('response_body').innerHTML=JSON.stringify(obj,undefined,1); // object to string
                if ( endpoint == 'rearrangement' ) { obj_rea=obj; } else { obj_rep=obj; }
            }
    }
    //if (method == 'POST'){ xhr.send(  document.getElementById('body_data').value ); }
    if (method == 'POST'){ xhr.send( body_post_data ); }
    else { xhr.send( ); }
    
}



// https://usefulangle.com/post/30/javascript-get-date-time-with-offset-hours-minutes
function getCurrentDateTime(){
    var dt = new Date(),
    current_date = dt.getDate(),
    current_month = dt.getMonth() + 1,
    current_year = dt.getFullYear(),
    current_hrs = dt.getHours(),
    current_mins = dt.getMinutes(),
    current_secs = dt.getSeconds(),
    current_datetime;

    // Add 0 before date, month, hrs, mins or secs if they are less than 0
    current_date = current_date < 10 ? '0' + current_date : current_date;
    current_month = current_month < 10 ? '0' + current_month : current_month;
    current_hrs = current_hrs < 10 ? '0' + current_hrs : current_hrs;
    current_mins = current_mins < 10 ? '0' + current_mins : current_mins;
    current_secs = current_secs < 10 ? '0' + current_secs : current_secs;

    // Current datetime
    // String such as 2016-07-16T19:20:30
    current_datetime = current_year + '-' + current_month + '-' + current_date + 'T' + current_hrs + ':' + current_mins + ':' + current_secs;
    return current_datetime;
}

function prepDownloadData(){
    var d=new Date();
    var fileName = 'ADCData-adc.dkfz.de-' + getCurrentDateTime() + '.json';
    var json = document.getElementById('response_body').innerHTML;
    var data = new Blob([json]);
    var a = document.getElementById('download_link');
    document.getElementById('download_link').download = fileName;
    document.getElementById('download_link').href = URL.createObjectURL(data);
    document.getElementById('displayDownloadLink').style = "display: block";
}

//Finds y value of given object
function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    return [curtop];
    }
}

function jumpto(h){
    //var top = document.getElementById(h).offsetTop; //Getting Y of target element
    //window.scrollTo(0, top);  
    //document.getElementById(h).scrollIntoView(true); 
    //location.href = "#";
    //location.href = "#" + h; 
    //window.scroll(0,findPos(document.getElementById(h)));
    var node = document.getElementById(h);
    //node.setAttribute('tabindex', '-1');
    //node.focus();
    //node.removeAttribute('tabindex');
    document.location.hash = "";
    document.location.hash = ("#" + h);

}

/*
    repertoire
    https://www.encodedna.com/javascript/populate-json-data-to-html-table-using-javascript.htm
    https://getbootstrap.com/docs/5.0/content/tables/
*/
function writeTableRep(obj){

    const airr_fields = ['repertoire_id', 'study.study_id', 'study.study_title', 'subject.subject_id']; 

    // (1) table 
    var table = document.createElement("table");
        table.setAttribute("id", "DisplayTableRep");
        table.setAttribute("class", "table table-light table-striped");
        //table.classList.add("table-info"); table.classList.add("table-striped");

    var thead = document.createElement("thead");    
        table.appendChild(thead);

    // (2) head row
    var trH = thead.insertRow(-1);               

    for ( var i = 0; i < airr_fields.length; i++ ){
        var th1 = document.createElement("th");
        th1.innerHTML = airr_fields[i];
        th1.setAttribute("scope","col");
        trH.appendChild(th1);        
    }

    var thAction = document.createElement("th");
    thAction.innerHTML="get rearr.";
    thAction.setAttribute("scope","col");
    trH.appendChild(thAction);    

    // (3) tbody 
    var tbody = document.createElement("tbody");    
        table.appendChild(tbody);

    // rows loop
    for (var i = 0; i < obj.Repertoire.length; i++) {
        var trD = tr = tbody.insertRow(-1);
        
        // col loop
        for ( var j = 0; j < airr_fields.length; j++ ){
            var tabCell1 = trD.insertCell(-1);
            var f = airr_fields[j];
            var felems = airr_fields[j].split(".");
            var value = "";
            // dot notation access with variables
            if (felems.length == 1)
            { 
                try { value = obj.Repertoire[i][f]; }
                catch (exception) { value = "-"; }
            }
            else if (felems.length == 2){ 
                try { value = obj.Repertoire[i][felems[0]][felems[1]]; }
                catch (exception) { value = "-"; }
            }
            else if (felems.length == 3){ 
                try { value = obj.Repertoire[i][felems[0]][felems[1]][felems[2]]; }
                catch (exception) { value = "-"; }
            }
            else if (felems.length == 4){ 
                try { value =  obj.Repertoire[i][felems[0]][felems[1]][felems[2]][felems[3]]; }
                catch (exception) { value = "-"; }
            }
            else if (felems.length == 5){ 
                try { value = obj.Repertoire[i][felems[0]][felems[1]][felems[2]][felems[3]][felems[4]]; }
                catch (exception) { value = "-"; }
            }
            tabCell1.innerHTML="<p style='font-size: 0.9rem'>" + value + "</p>"; 
            //tabCell1.innerHTML=obj.Repertoire[i]["study"]["study_id"];         
            //tabCell1.innerHTML=f;    
        }
		// action col
        var tabCellAction = trD.insertCell(-1);
        tabCellAction.innerHTML='<button type="button" class="btn btn-link" onClick="findReaForRepertoireId(\''+obj.Repertoire[i]['repertoire_id']+'\');">'
        + '<p style="font-size: 0.9rem">load</p></button>';        

    }


    // (4) add table to div
    var divContainer = document.getElementById("TableRep");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);
}

/*
    rearrangement
*/
function writeTableRea(obj){

}

function findReaForRepertoireId(repertoire_id){
    document.getElementById('body_data_rea').value='';
    document.getElementById('body_data_rea').value='' 
    + '{'
    + '    "filters":{'
    + '        "op": "=",'
    + '        "content": { "field":"repertoire_id", "value":"'+repertoire_id+'" }  '
    + '    }'
    + '}';
    getFromEnd('POST','rearrangement');
}

// - https://stackoverflow.com/questions/38552003/how-to-decode-jwt-token-in-javascript-without-using-a-library
function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    
    //  return JSON.stringify(jsonPayload,undefined,1);
    //return jsonPayload;
    return JSON.parse(jsonPayload);
}
